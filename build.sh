#!/usr/bin/env bash -x

mkdir -p $PREFIX/jar
# Drop the version # in the jar filename so the shell wrapper doesn't have to adjust.
cp ${PKG_NAME}-${PKG_VERSION}.jar $PREFIX/jar/${PKG_NAME}.jar

pwd
ls

mkdir -p $PREFIX/bin
cp ${RECIPE_DIR}/$PKG_NAME $PREFIX/bin/$PKG_NAME
